import timeit


def fibonacci_recursive(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci_recursive(n - 1) + fibonacci_recursive(n - 2)


def fibonacci_bottom_up(n):
    f0 = 0
    f1 = 1
    f2 = 1
    for i in range(1, n):
        f2 = f1 + f0
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    print(f"fibonacci recursive: {fibonacci_recursive(2)}")
    print(f"fibonacci bottom up: {fibonacci_bottom_up(2)}")
