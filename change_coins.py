def change_coins(coins=38):
    type_coins = [10, 5, 4, 2, 1]
    for coin in type_coins:
        count_coins = coins // coin
        coins %= coin
        if count_coins > 1:
            print(f"{coin:02d} bath: {count_coins} coins")
        elif count_coins == 1:
            print(f"{coin:02d} bath: {count_coins} coin")
        elif count_coins == 0:
            continue


if __name__ == "__main__":
    coins = int(input("Change coins:"))
    change_coins(coins)
