import random


def selection_sort(number_list):
    num_list = number_list
    len_num = len(num_list)
    for i in range(len_num, 0, -1):
        maxI = 0
        for j in range(1, i):
            if num_list[j] > num_list[maxI]:
                maxI = j
        num_list[i - 1], num_list[maxI] = num_list[maxI], num_list[i - 1]
    return num_list


if __name__ == "__main__":
    number = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    number_list = []
    for i in range(10):
        number_choice = random.choice(number)
        number_list.append(number_choice)
        number.remove(number_choice)

    print("Input to selection  sort function", number_list)
    print("selection  sort", selection_sort(number_list))
